package com.mazaiting.accessibilityservicetest;

import android.os.Handler;
import android.os.Looper;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class AccessibilityNormalSampleActivity extends AppCompatActivity {
  
  private Handler mHandler = new Handler(Looper.getMainLooper());
  private AccessibilityOperator accessibilityOperator;
  
  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_accessibility_normal_sample);
    accessibilityOperator = AccessibilityOperator.getInstance();
  }
  
  @Override protected void onResume() {
    super.onResume();
    // 执行延时任务
    clickText();
  }
  
  /**
   * 按文本点击
   */
  private void clickText() {
    clickTextItem("复选框",1);
    clickTextItem("单选按钮",2);
    clickTextItem("关闭",3);
    clickTextItem("退出本页面",4);
  }
  
  /**
   * 文本单个延时点击
   * @param text 文本内容
   * @param num 延时倍数
   */
  private void clickTextItem(final String text,int num) {
    mHandler.postDelayed(new Runnable() {
      @Override public void run() {
        final boolean isSuccess = accessibilityOperator.clickText(text);
        runOnUiThread(new Runnable() {
          @Override public void run() {
            popToast(isSuccess, text);
          }
        });
      }
    },2000*num);
  }
  
  /**
   * 弹出吐司
   * @param isSuccess
   * @param msg
   */
  private void popToast(boolean isSuccess, String msg) {
    if (isSuccess) {
      Toast.makeText(this, msg + "点击成功", Toast.LENGTH_SHORT).show();
    } else {
      Toast.makeText(this, msg + "点击失败", Toast.LENGTH_SHORT).show();
    }
  }
}
