package com.mazaiting.accessibilityservicetest;

import android.accessibilityservice.AccessibilityService;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

public class AccessibilitySampleService extends AccessibilityService {
  private static final String TAG = "AccessibilityService";
  @Override public void onAccessibilityEvent(AccessibilityEvent event) {
    Log.e(TAG, "onAccessibilityEvent: ");
    // 设置事件
    AccessibilityOperator.getInstance().updateEvent(this, event);
  }
  
  @Override public void onInterrupt() {
    Log.e(TAG, "onInterrupt: ");
  }
}
