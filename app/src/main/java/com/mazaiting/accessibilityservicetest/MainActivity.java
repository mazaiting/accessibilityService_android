package com.mazaiting.accessibilityservicetest;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    
    if (!OpenAccessibilitySettingHelper.isAccessibilitySettingsOn(this,
            AccessibilitySampleService.class.getName())) {// 判断服务是否开启
      OpenAccessibilitySettingHelper.jumpToSettingPage(this);// 跳转到开启页面
    } else {
      Toast.makeText(this, "服务已开启", Toast.LENGTH_SHORT).show();
    }
  }
  
  
  public void onStartNewActivity(View view) {
    startActivity(new Intent(this, AccessibilityNormalSampleActivity.class));
  }
}
